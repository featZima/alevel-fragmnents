package com.alevel.fragments;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity
        extends AppCompatActivity
        implements LoginFragment.LoginFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainer, new LoginFragment(), "fragment")
                    .commit();
        }
    }

    @Override
    public void onNavigateToNextScreen(String login) {
        MainFragment fragment = MainFragment.getInstance(login);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment, "fragment")
                .addToBackStack(null)
                .commit();
    }
}
