package com.alevel.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class LoginFragment extends Fragment {
    private final static String STATE_KEY_LOGIN = "login";
    private final static String STATE_KEY_PASSWORD = "password";

    private Button gotoNextScreenButton;
    private EditText loginEditText;
    private EditText passwordEditText;
    private LoginFragmentListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (LoginFragmentListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_login, container, false);
        loginEditText = fragmentView.findViewById(R.id.loginEditText);
        passwordEditText = fragmentView.findViewById(R.id.passwordEditText);

        gotoNextScreenButton = fragmentView.findViewById(R.id.gotoNextScreenButton);
        gotoNextScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onNavigateToNextScreen(loginEditText.getText().toString());
                }
            }
        });
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            loginEditText.setText(savedInstanceState.getString(STATE_KEY_LOGIN));
            loginEditText.setText(savedInstanceState.getString(STATE_KEY_PASSWORD));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_KEY_LOGIN, loginEditText.getText().toString());
        outState.putString(STATE_KEY_PASSWORD, passwordEditText.getText().toString());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    interface LoginFragmentListener {

        void onNavigateToNextScreen(String login);
    }
}
