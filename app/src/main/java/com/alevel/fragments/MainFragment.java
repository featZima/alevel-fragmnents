package com.alevel.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class MainFragment extends Fragment {

    private static final String ARG_LOGIN = "login";

    private TextView mainTitleTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_main, container, false);
        mainTitleTextView = fragmentView.findViewById(R.id.mainTitleTextView);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainTitleTextView.setText(getLogin());
    }

    private String getLogin() {
        return getArguments().getString(ARG_LOGIN);
    }

    public static MainFragment getInstance(String login) {
        Bundle arguments = new Bundle();
        arguments.putString(ARG_LOGIN, login);

        MainFragment fragment = new MainFragment();
        fragment.setArguments(arguments);

        return fragment;
    }
}
